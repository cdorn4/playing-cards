// Playing Cards
// Ian Gruender
//Chris Dorn

#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

enum Rank { Two = 2, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King, Ace };
enum Suit { Hearts = 1, Spades, Clubs, Diamonds };

struct Card
{
	Suit Suit;
	Rank Rank;
};

void PrintCard(Card card)
{
	cout << "The ";
	int rank = card.Rank;
	switch (rank)
	{
	case 2:
		cout << "two";
		break;
	case 3:
		cout << "three";
		break;
	case 4:
		cout << "four";
		break;
	case 5:
		cout << "five";
		break;
	case 6:
		cout << "six";
		break;
	case 7:
		cout << "seven";
		break;
	case 8:
		cout << "eight";
		break;
	case 9:
		cout << "nine";
		break;
	case 10:
		cout << "ten";
		break;
	case 11:
		cout << "Jack";
		break;
	case 12:
		cout << "Queen";
		break;
	case 13:
		cout << "King";
		break;
	case 14:
		cout << "Ace";
		break;
	default:
		break;
	}

	cout << " of ";

	int suit = card.Suit;
	switch (suit)
	{
	case 1:
		cout << "Hearts";
		break;
	case 2:
		cout << "Spades";
		break;
	case 3:
		cout << "Clubs";
		break;
	case 4:
		cout << "Diamonds";
		break;
	default:;
	};
		
};

Card HighCard(Card card1, Card card2) 

{
	Card Card1 = card1;
	Card Card2 = card2;

	if (Card1.Rank > Card2.Rank) 
	{
		PrintCard(Card1);
		return Card1;
	}
	else if (Card1.Rank < Card2.Rank)
	{
		PrintCard(Card2);
		return Card2;
	}
	else
	{
		cout << "Everybody Wins!";
		return Card1, Card2;
	}

}


int main()
{
	Card c1;
	c1.Suit = Spades;
	c1.Rank = Three;

	Card c2;
	c2.Suit = Hearts;
	c2.Rank = Eight;

	Card c3;
	c3.Suit = Diamonds;
	c3.Rank = Eight;

	Card c4;
	c4.Suit = Hearts;
	c4.Rank = Ace;

	PrintCard(c1); 

	cout << "\n";
	
	HighCard(c2, c4);

	(void)_getch();
	return 0;
}